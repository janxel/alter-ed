Alter-Ed development edition
============================

Instruction:
============

* Controller: (For the development, we will change after the final test.)
- Left Stick: Movement.
- LB, RB: Camera rotation
- Start: Restart.
- Select: Hide camera params.
- B: Jump
- A: Action
- X: Emotion change

* Keyboard:
- wasd: Movement.
- drag mouse while click left: Camera rotation (50% below the half of the screen)
- Enter: Restart.
- h: Hide camera params.
- space: Jump
- e: Action
- q: Emotion change
