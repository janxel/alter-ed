﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class pc2unityDemoScene : MonoBehaviour {
	public Transform LookAtTarget;
	public Transform ThisCamera;
	public int initialCount = 1; 
	public Vector3[] Positions;
	int[] PositionIndeces;
	List<GameObject> Instances;
	List<pc2unityController> InstancesControllers;
	public GameObject[] ObjectPrefab;
	public int GridSize;
	public float GridScale;

	public Rect AddButton;
	public Rect RemoveButton;

	public Rect LeftAxisButton;
	public Rect RightAxisButton;
	public Rect ForwardAxisButton;
	public Rect BackwardAxisButton;
	public Rect StatisticRect;
	public Rect NavigationRect;

	public Rect SpeedUpRect;
	public Rect SpeedDownRect;
	public Rect PlaybackLabelRect;
	public int PlaybackSpeed = 30;
	public int PlaybackSpeedMin = 25;
	public int PlaybackSpeedMax = 75;

	float xAxis;
	float yAxis;

	public float xSpeed = 5f;
	public float ySpeed = 2f;
	public bool ShowPlaybackSpeedButtons;

	float ButtonScale = 1;
	Vector2 leftBottomAnchor;
	Vector2 rightBottomAnchor;
	Vector2 topRightAnchor;
	string Statistic;
	int framesCounter;
	int fps;
	bool allowNewInput = true;

	public Rect SceneARect;
	public string SceneAName;
	public Rect SceneBRect;
	public string SceneBName;
	public Rect ScenesLabel;

	public bool RandomizeOrientation;
	public Vector3 ScaleRandom;
	public bool DisableGUI;

	void OnEnable () {
		Positions = new Vector3[GridSize*GridSize];
		Instances = new List<GameObject>();
		InstancesControllers = new List<pc2unityController>();
		float xstart = (GridSize-1)*GridScale*0.5f;
		float ystart = (GridSize-1)*GridScale*0.5f;
		for(int x = 0; x<GridSize; x++){
			for(int y = 0; y<GridSize; y++){
				Vector3 p = new Vector3(x*GridScale-xstart, 0, y*GridScale-ystart);
				int index = (x*GridSize)+y;
				Positions[index] = p;
			}
		}
		List<int> inList = new List<int>();
		List<int> outList = new List<int>();

		for(int i=0; i<Positions.Length; i++){
			inList.Add(i);
		}

		for(int i=0; i<Positions.Length; i++){
			int index = Random.Range(0, inList.Count);
			outList.Add(inList[index]);
			inList.RemoveAt(index);
		}
		PositionIndeces = outList.ToArray();
	}
	
	void Start(){
		Screen.autorotateToLandscapeLeft = true;
		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = true;
		if(Application.isPlaying){
			for(int i =0; i<initialCount;i++){
				Add();
			}
		}
		InvokeRepeating("ResetFramesCounter", 0, .5f);
	}

	void Update(){
		CamMovement();
		yAxis = Mathf.Lerp(yAxis, 0, 3f*Time.deltaTime);
		xAxis = Mathf.Lerp(xAxis, 0, 3f*Time.deltaTime);
		framesCounter ++;
		for(int i = 0; i<InstancesControllers.Count; i++){
			InstancesControllers[i].FramesPerSecond = PlaybackSpeed;
		}
	}

	void OnGUI(){
		if( DisableGUI )return;
		GUI.skin.button.fontSize = (int)(22*ButtonScale);
		GUI.skin.label.fontSize = (int)(22*ButtonScale);
		Statistic = Instances.Count.ToString() + " objects | "+ fps.ToString() +" FPS";

		GUI.Label(ARect(StatisticRect, new Vector2(0,0), ButtonScale), Statistic );
		ButtonScale = (float)Screen.height/(float)900f;
		leftBottomAnchor.Set(0, Screen.height - 180*ButtonScale);
		rightBottomAnchor.Set (Screen.width - 100*ButtonScale, Screen.height-100*ButtonScale );
		topRightAnchor.Set (Screen.width - 100*ButtonScale,0);

		GUI.Label(ARect (NavigationRect, leftBottomAnchor, ButtonScale), "navigation");
		if( GUI.Button(ARect( AddButton, new Vector2(0,0), ButtonScale), "Add (Z) ") || Input.GetKeyDown(KeyCode.Z)){
			if(allowNewInput){
				StartCoroutine("Delay");
				Add();
			}
		}
		if( GUI.Button(ARect( RemoveButton, new Vector2(0,0), ButtonScale), "Remove (X)")  || Input.GetKeyDown(KeyCode.X) ){
			if(allowNewInput){
				StartCoroutine("Delay");
				Remove();
			}
		}

		if(ShowPlaybackSpeedButtons){
			GUI.Label (ARect(PlaybackLabelRect, rightBottomAnchor, ButtonScale), "playback speed: "+PlaybackSpeed.ToString() );

			if(GUI.Button(ARect (SpeedDownRect, rightBottomAnchor, ButtonScale), "Down (C)") || Input.GetKey(KeyCode.C) ){
				if(allowNewInput){
					StartCoroutine("Delay");
					PlaybackSpeed = Mathf.Clamp (PlaybackSpeed-1, PlaybackSpeedMin, PlaybackSpeedMax);
				}
			}

			if(GUI.Button(ARect (SpeedUpRect, rightBottomAnchor, ButtonScale), "Up (V)") || Input.GetKey(KeyCode.V)  ){
				if(allowNewInput){
					StartCoroutine("Delay");
					PlaybackSpeed = Mathf.Clamp (PlaybackSpeed+1, PlaybackSpeedMin, PlaybackSpeedMax);
				}
			}
		}

		if(GUI.Button(ARect(SceneARect, topRightAnchor, ButtonScale), SceneAName)){
			//Application.LoadLevel(SceneAName);
		}

		if(GUI.Button(ARect(SceneBRect, topRightAnchor, ButtonScale), SceneBName)){
			//Application.LoadLevel(SceneBName);
		}

		if(GUI.RepeatButton( ARect( ForwardAxisButton, leftBottomAnchor, ButtonScale), "W") || Input.GetKey(KeyCode.W)){
			if(Vector3.Distance(ThisCamera.position, LookAtTarget.position)>2f ){
				yAxis = ySpeed;
			}
		} 

		if(GUI.RepeatButton( ARect( BackwardAxisButton, leftBottomAnchor, ButtonScale), "S") || Input.GetKey(KeyCode.S)){
			if(Vector3.Distance(ThisCamera.position, LookAtTarget.position)<12f ){
				yAxis = -ySpeed;
			}
		} 

		if(GUI.RepeatButton(ARect( LeftAxisButton, leftBottomAnchor, ButtonScale), "A") || Input.GetKey(KeyCode.A)){
			xAxis = xSpeed;
		} 

		if(GUI.RepeatButton(ARect( RightAxisButton, leftBottomAnchor, ButtonScale), "D") || Input.GetKey(KeyCode.D)){
			xAxis = -xSpeed;
		}

		GUI.Label(ARect(ScenesLabel, topRightAnchor, ButtonScale), "Demo Scenes:");
	}

	void ResetFramesCounter(){
		fps = framesCounter*2;
		framesCounter = 0;
	}

	void Add(){
		if(Instances.Count>=PositionIndeces.Length){
			return;
		}
		Quaternion r = Quaternion.identity;
		if(RandomizeOrientation){
			r = Quaternion.Euler(0,Random.Range(-180f, 180f),0);
		}
		GameObject nGO = Instantiate(ObjectPrefab[Random.Range(0,ObjectPrefab.Length)], Positions[ PositionIndeces[Instances.Count]], r) as GameObject;
		float rsx = Random.Range(-ScaleRandom.x, ScaleRandom.x);
		float rsy = Random.Range(-ScaleRandom.y, ScaleRandom.y);
		float rsz = Random.Range(-ScaleRandom.z, ScaleRandom.z);

		nGO.transform.localScale += new Vector3(rsx, rsy, rsz);
		InstancesControllers.Add(nGO.GetComponentsInChildren<pc2unityController>()[0]);
		Instances.Add(nGO);
	}

	void Remove(){
		if(Instances.Count<=0){
			return;
		}
		GameObject go = Instances[Instances.Count-1];
		InstancesControllers.Remove(go.GetComponentsInChildren<pc2unityController>()[0]);
		Instances.Remove(go);
		Destroy(go);
	}

	void OnDrawGizmos(){
		for(int i = 0; i<Positions.Length; i++){
			Gizmos.DrawCube(Positions[i], Vector3.one*0.1f);
		}
	}

	void CamMovement(){
		ThisCamera.LookAt(LookAtTarget);
		ThisCamera.RotateAround(Vector3.zero, Vector3.up, xAxis*Time.deltaTime);
		ThisCamera.position += (ThisCamera.forward*yAxis*Time.deltaTime);
	}


	Rect ARect(Rect r, Vector2 anchor, float scale){
		float x = (r.x * scale) + anchor.x;
		float y = (r.y * scale) + anchor.y;
		float w = r.width * scale;
		float h = r.height *scale;
		return new Rect(x, y, w, h);
	}

	IEnumerator Delay(){
		allowNewInput = false;
		yield return new WaitForSeconds(.1f);
		allowNewInput = true;
	}
}
