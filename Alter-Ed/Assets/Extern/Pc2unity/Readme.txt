1). Open pc2unity window at Window / pc2unity 

2) Choose your .obj file and .pc. 

3) Choose a location where the data asset will be stored.

4) After pressing the Import button will be created pc2unity object. 
You can change the parameters of the animation playback and export options.( Show Import Settings )


5)Tangent toggle - enable this option if your shader uses the normal map. 

6)If your shader not used normals, turn off the Normal toggle. This will reduce the memory usage. 

7)You can change the scale, orientation and offset relative transform pivot via Scale Factor, Orientation, Offset. 

8)Reverse toggle invert the sequence of frames. 

9) If you want to control the playback of animations manually or via your own script then set Playback Type enum to None and change the pc2unityController.Frames or assign  animation track on this variable.

10)You can save pc2unity object as regular prefab and Instantiate it. 
