using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Threading;
using UnityEditor.AnimatedValues;
using System.IO;

[CustomEditor(typeof(pc2unityController))]
public class Pc2unitycontrollerEditor : Editor {

	SerializedProperty frameProp;
	SerializedProperty minFramesProp;
	SerializedProperty maxFramesProp;
	SerializedProperty framesPerSeconds;
	SerializedProperty playProp;
	SerializedProperty scaleFactorProp;
	SerializedProperty yUpProp;
	SerializedProperty normalsProp;
	SerializedProperty tangentProp;
	SerializedProperty reverseProp;
	SerializedProperty pivotOffsetProp;
	SerializedProperty pivotRotationProp;

	float from;
	float to;
	bool isProccesing;
	static bool ExtraSettings;
	static AnimBool ExtraParamsAB;
	static pc2unityController t;



	void OnEnable(){
		t = (pc2unityController)target;
		frameProp = serializedObject.FindProperty("Frame");
		minFramesProp = serializedObject.FindProperty("MinFrame");
		maxFramesProp = serializedObject.FindProperty("MaxFrame");
		framesPerSeconds = serializedObject.FindProperty("FramesPerSecond");
		scaleFactorProp = serializedObject.FindProperty("Parameters.ScaleFactor");
		yUpProp = serializedObject.FindProperty("Parameters.YUp");
		normalsProp = serializedObject.FindProperty ("Parameters.CalcNormals");
		tangentProp = serializedObject.FindProperty ("Parameters.CalcTangents");
		reverseProp = serializedObject.FindProperty ("Parameters.ReverseTime");
		pivotOffsetProp = serializedObject.FindProperty("Parameters.PivotOffset");
		pivotRotationProp = serializedObject.FindProperty("Parameters.PivotRotation");
		ExtraParamsAB = new AnimBool(false, Repaint);
	}

	public override void OnInspectorGUI () {
		serializedObject.Update();
		GUI.skin.button.alignment = TextAnchor.MiddleRight;
		EditorGUILayout.EndFadeGroup();
		EditorGUILayout.Slider( frameProp, minFramesProp.floatValue, maxFramesProp.floatValue);
		string l = "Range "+ ((int)minFramesProp.floatValue).ToString()+ "-"+ ((int)maxFramesProp.floatValue).ToString();
		EditorGUILayout.MinMaxSlider(new GUIContent(l), ref t.MinFrame, ref t.MaxFrame, 0, t.GetFramesCount());
		minFramesProp.floatValue = t.MinFrame;
		maxFramesProp.floatValue = t.MaxFrame;
		t.PlaybackType =  (pc2unityController.PlaybackTypeEnum)EditorGUILayout.EnumPopup ("Playback Type:", t.PlaybackType );
		EditorGUILayout.IntSlider( framesPerSeconds, (int)1, (int)200);
		ExtraSettings = EditorGUILayout.Toggle("Show Import Settings", ExtraSettings);
		ExtraParamsAB.target = ExtraSettings;
		if(EditorGUILayout.BeginFadeGroup(ExtraParamsAB.faded)){
			GUI.skin.label.fontStyle = FontStyle.Bold;
			GUILayout.Label("Mesh:");
			EditorGUILayout.HelpBox (t.Parameters.ObjFilePath, MessageType.None);
			GUILayout.Label("Point Cache:");
			EditorGUILayout.HelpBox (t.Parameters.PC2FilePath, MessageType.None);
			GUILayout.Label("Output:");
			EditorGUILayout.HelpBox (t.Parameters.FramesDataPath, MessageType.None);
			GUI.skin.label.fontStyle = FontStyle.Normal;
			yUpProp.boolValue = EditorGUILayout.Toggle("Y Up", yUpProp.boolValue);
			normalsProp.boolValue = EditorGUILayout.Toggle("Normals", normalsProp.boolValue);
			tangentProp.boolValue = EditorGUILayout.Toggle("Tangents", tangentProp.boolValue);
			reverseProp.boolValue = EditorGUILayout.Toggle("Reverse Time", reverseProp.boolValue);


			scaleFactorProp.floatValue =  EditorGUILayout.FloatField("Scale Factor",scaleFactorProp.floatValue);
			pivotOffsetProp.vector3Value = EditorGUILayout.Vector3Field("Offset", pivotOffsetProp.vector3Value);
			pivotRotationProp.vector3Value = EditorGUILayout.Vector3Field("Rotation", pivotRotationProp.vector3Value);
			GUI.skin.button.alignment = TextAnchor.MiddleCenter;
			if(GUILayout.Button("Reload")){
				Reload(ref t.Parameters);
			} 
		
		}
		EditorGUILayout.EndFadeGroup();

		GUI.skin.button.alignment = TextAnchor.MiddleCenter;

		if (t!=null && t.Parameters.Frames!=null && t.Parameters.Frames [0] == null) {
			ReloadFrames(t.Parameters);
		}
		serializedObject.ApplyModifiedProperties();
	}


	public static void Reload (ref pc2unityImportSettings parameters) {
		new pc2unity.pc2unityMesh(parameters, false);
		t.MF.mesh = t.Parameters.Frames [(int)t.Frame];
	}


	void OnInspectorUpdate(){
		Repaint();
	}

	void ReloadFrames(pc2unityImportSettings settings){
		settings.FramesDataPath = AssetDatabase.GUIDToAssetPath (settings.FramesDataGuid);
		Object[] assets = AssetDatabase.LoadAllAssetsAtPath(settings.FramesDataPath);
		if(assets == null || assets.Length == 0) return;
		settings.Frames = new Mesh[assets.Length-1];
		for(int i = 0; i<assets.Length; i++){
			int index = -1;
			Mesh m = (Mesh)assets[i];
			if(m.name.StartsWith("frame #")){
				int.TryParse(m.name.Remove(0,7), out index);
				index = settings.ReverseTime? (settings.Frames.Length-index-1) : index; 
				settings.Frames[index] =  m;
			}
		}
	}


}
