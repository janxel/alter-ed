﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static partial class pc2unity {

	struct PointCacheFileDataOneFrame{
		public Vector3[] Positions;
	}

	struct PointCacheData {
		public int VerticesCount;
		public int FramesCount;
		public PointCacheFileDataOneFrame[] Frames;
	}

	static PointCacheData GetPointCacheData(string filePath, bool yUp ) {
		FileStream fs = new FileStream(filePath, FileMode.Open);
		BinaryReader binReader = new BinaryReader(fs);
		PointCacheData outputData;

		binReader.ReadChars(12); //signature
		binReader.ReadInt32(); //file version
		outputData.VerticesCount = binReader.ReadInt32();
		binReader.ReadSingle(); // start frame
        binReader.ReadSingle(); //sample rate
		outputData.FramesCount = binReader.ReadInt32();

		outputData.Frames = new PointCacheFileDataOneFrame[ outputData.FramesCount ];
		for (int f = 0; f < outputData.Frames.Length; f++) {
			outputData.Frames[f].Positions = new Vector3[outputData.VerticesCount];
			for (int p = 0; p < outputData.VerticesCount; p++) {
				outputData.Frames[f].Positions[p].x = binReader.ReadSingle();
				if(yUp){
					outputData.Frames[f].Positions[p].z = binReader.ReadSingle();
					outputData.Frames[f].Positions[p].y = binReader.ReadSingle();
				} else {
					outputData.Frames[f].Positions[p].y = binReader.ReadSingle();
					outputData.Frames[f].Positions[p].z = binReader.ReadSingle();
				}
			} 
		}
		return outputData;
	}
}
