﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public static partial class pc2unity {

	#region CreatePolygonsList
	static void CreatePolygonsList(ref ObjData data, ref List<Polygon> Polygons){
		for(int p = 0; p<data.Polygons.Count; p++){
			Polygon polygon = new Polygon();
			polygon.Corners =  new List<int>();
			polygon.SubMeshIndex = data.Polygons[p].MatIndex;
			if(polygon.SubMeshIndex<0){
				polygon.SubMeshIndex = 0;
			}
			Polygons.Add(polygon);
		}
	}
	#endregion

	#region SubMeshesList
	static void CreateSubMeshesLists(ref ObjData data, ref List<SubMesh> SubMeshes){
		for(int i = 0; i<data.Materials.Count; i++){
			SubMesh submesh = new SubMesh();
			submesh.Triangles = new List<int>();
			submesh.MaterialName = data.Materials[i].Name;
			SubMeshes.Add( submesh );
		}
		if(data.Materials.Count == 0){
			SubMesh submesh = new SubMesh();
			submesh.Triangles = new List<int>();
			submesh.MaterialName = "DefaultMat";
			SubMeshes.Add( submesh );
		}
	}
	#endregion

	#region PolygonToTriangles
	static void PolygonToTriangles(ref List<SubMesh> subMeshes, Polygon polygon, bool inverted, ref int trisCounter){
		for(int i = 0; i<polygon.Corners.Count-2; i++){
			if(inverted){
				subMeshes[polygon.SubMeshIndex].Triangles.Add(polygon.Corners[i+2]);
				subMeshes[polygon.SubMeshIndex].Triangles.Add(polygon.Corners[i+1]);
				subMeshes[polygon.SubMeshIndex].Triangles.Add(polygon.Corners[0]);
			} else {
				subMeshes[polygon.SubMeshIndex].Triangles.Add(polygon.Corners[0]);
				subMeshes[polygon.SubMeshIndex].Triangles.Add(polygon.Corners[i+1]);
				subMeshes[polygon.SubMeshIndex].Triangles.Add(polygon.Corners[i+2]);
			}
			trisCounter ++;
		}
	}
	#endregion

	#region AddSV
	static void AddSV(ref List<SubVertex> svList, SubVertex sv, ref int indexOfAddedSV ){
		int index = -1;
		for(int i = 0; i<svList.Count; i++){
			bool isIdentity =  svList[i].ControlVertexIndex == sv.ControlVertexIndex && svList[i].UVIndex == sv.UVIndex && svList[i].SmoothingGroupIndex == sv.SmoothingGroupIndex;
			if(isIdentity){
				index = i ;
				break;
			}
		}
		if(index<0){
			svList.Add(sv);
			index = svList.Count-1;
		}
		indexOfAddedSV = index;
	}
	#endregion

	#region CalculateNormals
	static void CalculateNormals(ref Mesh mesh, ref List<NormalNode> normalNodes){
		mesh.RecalculateNormals();
		Vector3[] normals = mesh.normals;
		for(int n = 0; n<normalNodes.Count; n++){
			Vector3 normal = Vector3.zero;
			float mult = 1f/(float)normalNodes[n].AttachedSubVertices.Count;
			for(int s = 0; s<normalNodes[n].AttachedSubVertices.Count; s++){
				int svIndex = normalNodes[n].AttachedSubVertices[s];
				normal += normals[svIndex]*mult;
			}
			for(int s = 0; s<normalNodes[n].AttachedSubVertices.Count; s++){
				int svIndex = normalNodes[n].AttachedSubVertices[s];
				normals[svIndex] = normal;
			}
		}
		mesh.normals = normals;
	}
	#endregion


	#region CalculateTangents
	static void CalcTangents(ref Mesh m) {
		Vector3[] vertices = m.vertices;
		Vector3[] normals = m.normals;
		Vector2[] uvs = m.uv;
		int[] tris = m.triangles;
		Vector4[] tangents = new Vector4[m.vertexCount];
		Vector3[] t1 = new Vector3[m.vertexCount];
		Vector3[] t2 = new Vector3[m.vertexCount];
		Vector3 dir1 = Vector3.zero, dir2 = Vector3.zero, vec1 = Vector3.zero, vec2 = Vector3.zero;
		int tv0 = 0, tv1 = 0, tv2 = 0;
		Vector2 uvvec1 = Vector2.zero, uvvec2 = Vector2.zero ;
		float l;
		
		for (int i = 0; i < tris.Length; i+=3) {
			tv0 = tris[i];
			tv1 = tris[i+1];
			tv2 = tris[i+2];
			vec1 = vertices[tv1]-vertices[tv0];
			vec2 = vertices[tv2]-vertices[tv0];
			uvvec1 = uvs[tv1]-uvs[tv0];
			uvvec2 = uvs[tv2]-uvs[tv0];
			l = 1f / (uvvec1.x * uvvec2.y - uvvec2.x * uvvec1.y);
			dir1.Set ((uvvec2.y * vec1.x - uvvec1.y * vec2.x) , (uvvec2.y * vec1.y - uvvec1.y * vec2.y) , (uvvec2.y * vec1.z - uvvec1.y * vec2.z) );
			dir2.Set ((uvvec1.x * vec2.x - uvvec2.x * vec1.x) , (uvvec1.x * vec2.y - uvvec2.x * vec1.y) , (uvvec1.x * vec2.z - uvvec2.x * vec1.z) );
			dir1 *=l;
			dir2 *=l;
			t1[tv0] = t1[tv1] = t1[tv2] = dir1;
			t2[tv0] = t2[tv1] = t2[tv2] = dir2;
		}
		
		for (int i = 0; i < m.vertexCount; i++) {
			Vector3.OrthoNormalize(ref normals[i], ref t1[i]);
			tangents[i] = t1[i];
			tangents[i].w = ( Vector3.Dot(Vector3.Cross(normals[i], t1[i]), t2[i]) < 0 ) ? -1f : 1f;
			
		}      
		m.tangents = tangents;
	}
	#endregion

//	static bool VertexEquals(Vector3 a, Vector3 b){
//		return Vector3.Distance(a,b)<0.0001f;
//	} 
}



