using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using UnityEditor.AnimatedValues;

public class Pc2unityWindow : EditorWindow {
	#region SourceFile
	[System.Serializable]
	class Pc2unitySourceFile{
		public string FileExtension;
		public FileSystemInfo ThisFile;
		public string resentDirectory;
		public string resentFile;
		public string PrefKeyName;
		public string SelectDialogInfo;
		public string OnWrongExtensionAlertText;
		public string[] InfoStrings;
		
		public Pc2unitySourceFile( string prefKeyName){
			PrefKeyName = prefKeyName;
			resentDirectory = EditorPrefs.GetString(PrefKeyName);
			resentFile = EditorPrefs.GetString(PrefKeyName+"file");
			if(File.Exists(resentFile)){
				ThisFile = new FileInfo(resentFile);
			}
			FileExtension = "";
			SelectDialogInfo = "";
			OnWrongExtensionAlertText ="";
			InfoStrings = null;
		}
		
		public string GetFileName(){
			string o = "<none>";
			if(ThisFile!=null){
				o = ThisFile.Name.Remove(ThisFile.Name.Length-ThisFile.Extension.Length);
			}
			return o;
		}
		
		public bool SelectFile(){
			bool r = false;
			string tempString = "";
			tempString = EditorUtility.OpenFilePanel(SelectDialogInfo, resentDirectory , FileExtension.Remove(0,1 ));
			if(tempString!=null && tempString.Length>0){
				FileSystemInfo selectedFile ;
				selectedFile = new FileInfo (tempString);
				if( selectedFile.Extension!=FileExtension ){
					EditorUtility.DisplayDialog ("Wrong file extension", OnWrongExtensionAlertText, "OK" );
					return false;
				}
				resentDirectory = Path.GetDirectoryName(tempString);
				resentFile = tempString;
				ThisFile = selectedFile;
				EditorPrefs.SetString(PrefKeyName, resentDirectory);
				EditorPrefs.SetString(PrefKeyName+"file", resentFile);
				r = true;
			} else {
				ThisFile = null;
			}
			return r;
		}
	}
	#endregion

	#region declaration
	AnimBool ShowObjInfoBox;
	AnimBool ShowPcInfoBox;
	AnimBool ShowOutput;
	AnimBool ShowImportButton;

	[SerializeField]
	static Pc2unitySourceFile ObjFile;
	[SerializeField]
	static Pc2unitySourceFile PcFile;

	pc2unity.ObjFileInfo ObjInfo;
	pc2unity.PointCacheFileInfo PCInfo;
	static int yOffset;
	static int buttonWidth; 

	static bool isValidPair = false;
	static pc2unityImportSettings parameters;
	static string outputPath;
	static bool createMaterials = true;
	#endregion
	static Pc2unityWindow  win;

	#region Init
	[MenuItem("Window/pc2unity")]
	static void Init(){ 
		win =  (Pc2unityWindow)EditorWindow.GetWindow (typeof (Pc2unityWindow));
		win.minSize = new Vector2(200,150);
	}
	#endregion

	void CreateObjSource(){
		ObjFile = new Pc2unitySourceFile("pc2unityObjSourcePath");
		ObjFile.FileExtension = ".obj";
		ObjFile.OnWrongExtensionAlertText = "File must have .obj extension";
		ObjFile.SelectDialogInfo = "Select .obj file";
	}

	void CreatePcSource(){
		PcFile = new Pc2unitySourceFile("pc2unityPCSourcePath");
		PcFile.FileExtension = ".pc2";
		PcFile.OnWrongExtensionAlertText = "File must have .pc2 extension";
		PcFile.SelectDialogInfo = "Select .pc2 file";
	}

	#region OnEnable
	void OnEnable(){
		//title = "pc2unity";
        titleContent = new GUIContent("pc2unity");
		CreateObjSource ();
		CreatePcSource ();
		ShowObjInfoBox = new AnimBool(false, Repaint);
		ShowPcInfoBox = new AnimBool(false, Repaint);
		ShowOutput = new AnimBool (false, Repaint);
		ShowImportButton = new AnimBool(false, Repaint);
		parameters = new pc2unityImportSettings();
		parameters.ScaleFactor = 1;
		parameters.CalcNormals = true;
	}
	#endregion

	#region OnDestroy
	void OnDestroy(){
		ShowObjInfoBox.target = false;
		ShowPcInfoBox.target = false;
	}
	#endregion

	void OnGUI(){
		#region ObjButton
		EditorGUILayout.BeginHorizontal("label");
		EditorGUILayout.LabelField("Mesh:", GUILayout.Width(72));
		if( GUILayout.Button(ObjFile.GetFileName())){
			if(ObjFile.SelectFile()){
				ObjInfo = pc2unity.GetObjInfo(ObjFile.ThisFile.FullName);
			} 
		}
		if(ObjFile.ThisFile!=null && ObjInfo== null){
			ObjInfo = pc2unity.GetObjInfo(ObjFile.ThisFile.FullName);
		}
		EditorGUILayout.LabelField(".obj", GUILayout.Width(30));
		EditorGUILayout.EndHorizontal();
		ShowObjInfoBox.target = ObjFile.ThisFile!=null;
		if(EditorGUILayout.BeginFadeGroup(ShowObjInfoBox.faded)){
			string str =  "object:  "+ObjInfo.ObjectName;
			str +=  "\nvertices:  " + ObjInfo.VerticesCount.ToString();
			str += "\nsmoothing groups:  " + ObjInfo.SmoothingGroupCount.ToString();
			str += "\noff smoothing groups:  " + ObjInfo.OffSmothingGroups.ToString();
			str += "\nmaterials (submeshes):  "  + ObjInfo.Materials.Length.ToString();
			EditorGUILayout.HelpBox(str ,MessageType.None);
		}
		EditorGUILayout.EndFadeGroup();
		#endregion

		#region PCButton
		EditorGUILayout.BeginHorizontal("label");
		EditorGUILayout.LabelField("PointCache:", GUILayout.Width(72));
		if(GUILayout.Button(PcFile.GetFileName())){
			if(PcFile.SelectFile()){
				PCInfo = pc2unity.GetPointCacheFileInfo(PcFile.ThisFile.FullName);
			}
		}

		if(PcFile.ThisFile!=null && PCInfo==null){
			PCInfo = pc2unity.GetPointCacheFileInfo(PcFile.ThisFile.FullName);
		}
		EditorGUILayout.LabelField(".pc2", GUILayout.Width(30));
		EditorGUILayout.EndHorizontal();
		ShowPcInfoBox.target = PcFile.ThisFile!=null;
		if(PCInfo!=null && ObjInfo!=null){
			isValidPair = PCInfo.VertexCount == ObjInfo.VerticesCount;
		} else {
			isValidPair = false;
		}
		if(EditorGUILayout.BeginFadeGroup(ShowPcInfoBox.faded)){
			string str = "vertices:" + PCInfo.VertexCount.ToString()+ "\n";
			str += "frames:" + PCInfo.Frames.ToString()+ "\n";
			str += "bounds: " + PCInfo.Dimensions.x.ToString("F2") + "x"+PCInfo.Dimensions.y.ToString("F2") + "x"+PCInfo.Dimensions.z.ToString("F2");
			MessageType mt = MessageType.None;
			if(!isValidPair){
				mt = MessageType.Error;
				str =  "Vertex count mismatch!\n\n"+str;
			}
			EditorGUILayout.HelpBox(str ,mt);
		}
		EditorGUILayout.EndFadeGroup();
		#endregion

		#region OutputButton
		ShowOutput.target = (ObjFile.ThisFile!=null) && (PcFile.ThisFile!=null) && isValidPair;
		if(EditorGUILayout.BeginFadeGroup(ShowOutput.faded)){
			EditorGUILayout.BeginHorizontal("label");
			EditorGUILayout.LabelField("Output :", GUILayout.Width(72));
			if(GUILayout.Button(GetOutputButtonName())){
				string assetPath = EditorUtility.SaveFilePanelInProject("select output asset", outputPath, "asset", "save");
				if(assetPath!=null && assetPath.Length!=0){
					AssetDatabase.CreateAsset( new Mesh(), assetPath);
					parameters.FramesDataGuid = AssetDatabase.AssetPathToGUID(assetPath);
					parameters.OutputName = Path.GetFileNameWithoutExtension(assetPath);
					outputPath = assetPath;
					AssetDatabase.Refresh();
				}
			}
			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndFadeGroup();
		#endregion

		#region ImportButton
		ShowImportButton.target = outputPath!=null && outputPath.Length>0;
		if(EditorGUILayout.BeginFadeGroup(ShowImportButton.faded)){
//			parameters.ScaleFactor = EditorGUILayout.FloatField("Scale Factor", parameters.ScaleFactor);
//			parameters.YUp = EditorGUILayout.Toggle("Y Up", parameters.YUp);
//			parameters.CalcNormals = EditorGUILayout.Toggle("Normals", parameters.CalcNormals);
//			parameters.CalcTangents =  EditorGUILayout.Toggle("Tangents", parameters.CalcTangents);
//			parameters.ReverseTime = EditorGUILayout.Toggle("Reverse Time", parameters.ReverseTime);
			createMaterials = EditorGUILayout.Toggle("Create Materials", createMaterials);
			EditorGUILayout.BeginHorizontal("label");
			if(GUILayout.Button("Import")){
				parameters.ObjFilePath = ObjFile.ThisFile.FullName;
				parameters.PC2FilePath = PcFile.ThisFile.FullName;
				GameObject go = new GameObject( parameters.OutputName );
				pc2unityController controller =  go.AddComponent<pc2unityController>();
				controller.MF = go.AddComponent<MeshFilter>();
				controller.MR = go.AddComponent<MeshRenderer>();
				parameters.ThisController = controller;
				parameters.ThisTransform = go.transform;
				controller.Parameters = parameters;
				new pc2unity.pc2unityMesh(parameters, createMaterials);
				controller.MinFrame = 0;
				controller.MaxFrame = controller.Parameters.Frames.Length-1;
				controller.FramesPerSecond = 30;
			}
			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndFadeGroup();
		#endregion

	}

	string GetOutputButtonName(){
		string s = "<none>";
		if(outputPath!=null && outputPath.Length>0){
			s = outputPath;
		}
		return s;
	}






}
