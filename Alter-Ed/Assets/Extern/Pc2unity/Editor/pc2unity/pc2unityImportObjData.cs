﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;



public static partial class pc2unity {
	#region ObjDataDeclaration
	struct ObjData{
		public string ObjectName;
		public List<Vector3> Vertices;
		public List<Vector2> UVs;
		public List<ObjPolygon> Polygons;
		public List<ObjMaterial> Materials;
		public List<ObjSmoothingGroup> SmoothingGroups;
	}
	
	struct ObjSmoothingGroup{
		public string Name;
		public ObjSmoothingGroup (string name){
			Name = name;
		}
	}
	
	struct ObjPolygon{
		public List<ObjPolygonCorner> Corners; 
		public int MatIndex;
		public int SmoothingGroupIndex;
	}
	
	struct ObjPolygonCorner{
		public int VertexIndex ;
		public int UVIndex;
	}
	
	struct ObjMaterial{
		public string Name;
		public string TexturePath;
		
		public ObjMaterial(string name){
			Name = name;
			TexturePath = null;
		}
	}
	#endregion

	static ObjData ImportObjData(string path){
		System.IO.TextReader objFile = System.IO.File.OpenText(path);
		ObjData data = new ObjData();
		data.Vertices = new List<Vector3>();
		data.UVs = new List<Vector2>();
		data.SmoothingGroups = new List<ObjSmoothingGroup>();
		data.Materials = new List<ObjMaterial>();
		data.Polygons = new List<ObjPolygon>();
		bool currentHasSmoothingGroup = false;
		int currentSmoothingGroupIndex = 0;
		int currentMatIndex = -1;
		int stringCounter = 0;
		int offSmoothingGroupIndex = 0;
		while (true){
			string str = objFile.ReadLine();
			if(str == null) break;
			#region uvs
			if(str.StartsWith("vt ")){
				Vector2 uv = new Vector2();
				MatchCollection matches = Regex.Matches (str, @"\-?\d[-.0-9]+");
				float.TryParse (matches[0].Value, out uv.x);
				float.TryParse (matches[1].Value, out uv.y);
				data.UVs.Add(uv); 
			#endregion
			#region vertices 
			} else if (str.StartsWith("v ")){
				Vector3 v = new Vector3();
				MatchCollection matches = Regex.Matches (str, @"\-?\w+(.)\w+");
				float.TryParse (matches[0].Value, out v.x);
				float.TryParse (matches[1].Value, out v.y);
				float.TryParse (matches[2].Value, out v.z);
				data.Vertices.Add(v);
			#endregion
			#region Polygons
			} else if (str.StartsWith("f ") ){
				ObjPolygon p = new ObjPolygon();
				p.MatIndex = currentMatIndex;
				p.Corners = new List<ObjPolygonCorner>();
				MatchCollection cornerMatches = Regex.Matches (str, @"[0-9/]+" );
				for (int m = 0; m <cornerMatches.Count; m++ ){
					string[] oneSubVertPair = cornerMatches[m].Value.Split(new string[1]{@"/"}, System.StringSplitOptions.None);
					ObjPolygonCorner corner = new ObjPolygonCorner();
					int.TryParse( oneSubVertPair[0], out corner.VertexIndex);
					corner.VertexIndex --;
					if(oneSubVertPair.Length>1)
					int.TryParse( oneSubVertPair[1], out corner.UVIndex);
					corner.UVIndex --;
					p.Corners.Add(corner);
				}
				if( !currentHasSmoothingGroup ){
					data.SmoothingGroups.Add( new ObjSmoothingGroup("off #"+ offSmoothingGroupIndex.ToString()));
					offSmoothingGroupIndex ++;
					currentSmoothingGroupIndex = data.SmoothingGroups.Count-1;
				}
				p.SmoothingGroupIndex = currentSmoothingGroupIndex;
				data.Polygons.Add(p);

			#endregion
			#region smoothingGroup
			} else if( str.StartsWith ("s off")){
				currentHasSmoothingGroup = false;
			} else if ( str.StartsWith ("s " ) ){
				currentHasSmoothingGroup = true;
				bool smoothingGroupExist = false;
				for(int i = 0; i<data.SmoothingGroups.Count; i++ ){
					if(data.SmoothingGroups[i].Name == str){
						currentSmoothingGroupIndex = i;
						smoothingGroupExist = true;
						break;
					}
				}
				if(!smoothingGroupExist){
					data.SmoothingGroups.Add(new ObjSmoothingGroup(str));
					currentSmoothingGroupIndex = data.SmoothingGroups.Count-1;
				}
			}
			#endregion
			#region materials
			else if (str.StartsWith("usemtl")){
				string matName = str.Remove(0,7);
				bool exist = false;
				for(int i = 0; i<data.Materials.Count; i++){
					if( data.Materials[i].Name == matName){
						exist = true;
						currentMatIndex = i;
						break;
					} 
				}
				if(!exist){
					data.Materials.Add(new ObjMaterial(matName));
					currentMatIndex = data.Materials.Count-1;
				}
			}
			#endregion
			stringCounter ++;
		}
		if(data.UVs.Count == 0){
			data.UVs.Add(Vector2.zero);
		}
		return data;
	}
}


