﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

public static partial class pc2unity {
	public class ObjFileInfo{
		public string ObjectName;
		public int VerticesCount;
		public int SmoothingGroupCount;
		public int OffSmothingGroups;
		public string[] Materials;
	}


	public static ObjFileInfo GetObjInfo(string path){
		System.IO.TextReader objFile = System.IO.File.OpenText(path);
		ObjFileInfo info = new ObjFileInfo();
		List<string> sg = new List<string>();
		List<string> mats = new List<string>();
		bool currentHasSmoothingGroup = false;
		while (true){
			string str = objFile.ReadLine();
			if(str == null) break;
			if(str.StartsWith("# object")){
				info.ObjectName = str.Remove(0,8);
			} else if (str.StartsWith("v ")){
				info.VerticesCount ++;				
			} else if( str.StartsWith ("s off")){
				currentHasSmoothingGroup = false;
			} else if ( str.StartsWith ("s " ) ){
				currentHasSmoothingGroup = true;
				bool smoothingGroupExist = false;
				for(int i = 0; i<sg.Count; i++ ){
					if(sg[i] == str){
						smoothingGroupExist = true;
						break;
					}
				}
				if(!smoothingGroupExist){
					sg.Add( str );
				}
			} else if( currentHasSmoothingGroup == false && str.StartsWith("f ")){
				info.OffSmothingGroups ++;
			}
			else if (str.StartsWith("usemtl")){
				string matName = str.Remove(0,7);
				bool exist = false;
				for(int i = 0; i<mats.Count; i++){
					if( mats[i] == matName){
						exist = true;
						break;
					} 
				}
				if(!exist){
					mats.Add( matName );
				}
			}
			info.Materials = mats.ToArray();
			info.SmoothingGroupCount = sg.Count;
		}
		return info;
	}

	[System.Serializable]
	public class PointCacheFileInfo{
		public int VertexCount;
		public int Frames;
		public Vector3 Dimensions;
	}
	
	public static PointCacheFileInfo GetPointCacheFileInfo(string filePath ) {
		FileStream fs = new FileStream(filePath, FileMode.Open);
		BinaryReader binReader = new BinaryReader(fs);
		PointCacheFileInfo pcinfo = new PointCacheFileInfo();
		binReader.ReadChars(12);
		binReader.ReadInt32();
		pcinfo.VertexCount = binReader.ReadInt32();
		binReader.ReadSingle();//start frame
		binReader.ReadSingle();//sample rate
		pcinfo.Frames = binReader.ReadInt32();
		pcinfo.Dimensions = new Vector3();
		Bounds b  = new Bounds(new Vector3(binReader.ReadSingle(), binReader.ReadSingle(), binReader.ReadSingle()), Vector3.zero);
		Vector3 tVec = new Vector3();
		for(int f = 0; f<pcinfo.Frames; f++){
			for (int p = 1; p < pcinfo.VertexCount; p++) {
				tVec.x = binReader.ReadSingle();
				tVec.y = binReader.ReadSingle();
				tVec.z = binReader.ReadSingle();
				b.Encapsulate(tVec);
			}
		}
		pcinfo.Dimensions = b.size;
		fs.Close();
		binReader.Close();
		return pcinfo;
	}
}


