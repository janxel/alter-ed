using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;

public static partial class pc2unity  {

	struct SubVertex{
		public int ControlVertexIndex;
		public int UVIndex;
		public int SmoothingGroupIndex;
		public int NormalNodeIndex;
	}
		
	struct Polygon{
		public List<int> Corners; 
		public int SubMeshIndex;
		public Vector3 Normal;
	}
		
	struct NormalNode {
		public int SmoothingGroupIndex;
		public int ControlVertexIndex;
		public List<int> AttachedSubVertices;
	}
	
	struct SubMesh{
		public string MaterialName;
		public List<int> Triangles;
	}

	[System.Serializable]
	public class pc2unityMesh{
		List<SubVertex> SubVertices;
		List<Polygon> Polygons;
		List<NormalNode> NormalNodes;
		List<SubMesh> SubMeshes;
		PointCacheData pc;
		ObjData obj;
		int trisCounter;
		public pc2unityImportSettings parameters;

		public pc2unityMesh(pc2unityImportSettings pr , bool createMaterials){
			float progress = 0;
			parameters = pr;

			if(!File.Exists(parameters.ObjFilePath)){
				EditorUtility.DisplayDialog ("Source obj file not found", "", "OK" );
				return;
			}
			if(!File.Exists(parameters.PC2FilePath)){
				EditorUtility.DisplayDialog ("Source pc2 file not found", "", "OK" );
				return;
			}

			parameters.FramesDataPath = AssetDatabase.GUIDToAssetPath(parameters.FramesDataGuid);

			Object o = AssetDatabase.LoadAssetAtPath (parameters.FramesDataPath, typeof(Object));
			if(o == null){
				EditorUtility.DisplayDialog ("Frames asset not found", "", "OK" );
				return;
			}
			AssetDatabase.CreateAsset( new Mesh(), parameters.FramesDataPath);
			parameters.FramesDataGuid = AssetDatabase.AssetPathToGUID(parameters.FramesDataPath);

			obj = ImportObjData(parameters.ObjFilePath);
			pc = GetPointCacheData(parameters.PC2FilePath, parameters.YUp);

			if(obj.Vertices.Count != pc.VerticesCount){
				EditorUtility.DisplayDialog ("Vertex count mismatch", "Vertex count in obj and pc2 file must be identical", "OK" );
				return;
			}

			SubVertices = new List<SubVertex>();
			Polygons = new List<Polygon>();
			NormalNodes = new List<NormalNode>();
			SubMeshes = new List<SubMesh>();
			trisCounter = 0;
			CreatePolygonsList(ref obj, ref Polygons);
			CreateSubMeshesLists(ref obj, ref SubMeshes);

			parameters.Frames = new Mesh [pc.FramesCount];
			int progressSteps = obj.Polygons.Count/100;
			int progressStepAccum = 0;
			EditorUtility.DisplayProgressBar("Importing", "Bulding mesh polygons: 0", .1f);
			for(int p = 0; p<obj.Polygons.Count; p++){
				for(int c =0; c<obj.Polygons[p].Corners.Count; c++ ){
					SubVertex temp = new SubVertex();
					temp.ControlVertexIndex = obj.Polygons[p].Corners[c].VertexIndex;
					temp.UVIndex = obj.Polygons[p].Corners[c].UVIndex;
					temp.SmoothingGroupIndex = obj.Polygons[p].SmoothingGroupIndex;
					int currentSVindex = -1;
					AddSV (ref SubVertices, temp, ref currentSVindex) ;
					Polygons[p].Corners.Add ( currentSVindex );
				}
				progressStepAccum ++;
				if(progressStepAccum>=progressSteps){
					float _progress = ((float)p/obj.Polygons.Count)*0.4f;
					progress = _progress;
					EditorUtility.DisplayProgressBar("Importing", "Bulding mesh polygons: "+p.ToString(), progress);
					progressStepAccum = 0;
				}
			}

			for(int p = 0; p<obj.Polygons.Count; p++){
				PolygonToTriangles(ref SubMeshes, Polygons[p], parameters.YUp, ref trisCounter);
			}
	
			progressSteps = SubVertices.Count/100;
			progressStepAccum = 0;
			for (int s = 0; s<SubVertices.Count; s++){
				int findedIndex = -1;
				for(int n = 0; n<NormalNodes.Count; n++){
					bool isIdentity = SubVertices[s].ControlVertexIndex == NormalNodes[n].ControlVertexIndex && SubVertices[s].SmoothingGroupIndex == NormalNodes[n].SmoothingGroupIndex;
					if(isIdentity){
						NormalNodes[n].AttachedSubVertices.Add( s );
						findedIndex = n;
						break;
					}
				}
				if(findedIndex<0){
					NormalNode newNormalNode = new NormalNode();
					newNormalNode.AttachedSubVertices = new List<int>();
					newNormalNode.ControlVertexIndex = SubVertices[s].ControlVertexIndex;
					newNormalNode.SmoothingGroupIndex = SubVertices[s].SmoothingGroupIndex;
					newNormalNode.AttachedSubVertices.Add ( s );
					NormalNodes.Add(newNormalNode);
				}
				progressStepAccum++;
				if(progressStepAccum>=progressSteps){
					float _progress = ((float)s/SubVertices.Count )*0.2f;
					progress = .4f+_progress;
					EditorUtility.DisplayProgressBar("Importing", "Bulding mesh vertices: "+s.ToString(), progress);
					progressStepAccum = 0;
				}
			}
			
			Vector3[] vertices = new Vector3[SubVertices.Count];
			Vector2[] uvs = new Vector2[SubVertices.Count];
			for(int i = 0; i<SubVertices.Count; i++){
				uvs[i] = obj.UVs[ SubVertices[i].UVIndex ];
			}
			Quaternion pivotRot = Quaternion.Euler(parameters.PivotRotation);
			for(int f = 0; f<pc.FramesCount; f++){
				Mesh tm = new Mesh();
				tm.name = "frame #" + f.ToString();
				tm.vertices = vertices;
				tm.uv = uvs;
				tm.subMeshCount = SubMeshes.Count;
				for(int i = 0; i<SubMeshes.Count; i++){
					tm.SetTriangles( SubMeshes[i].Triangles.ToArray(),i);
				}
				Vector3[] tempVerticesArr = new Vector3[SubVertices.Count];
				for(int i = 0; i<tempVerticesArr.Length; i++){
					int pcIndex = SubVertices[i].ControlVertexIndex;
					tempVerticesArr[i] =  (pc.Frames[f].Positions[pcIndex]*parameters.ScaleFactor) + -parameters.PivotOffset;
					tempVerticesArr[i] = pivotRot*tempVerticesArr[i];
				}

				tm.vertices = tempVerticesArr;
				if(parameters.CalcNormals){
					CalculateNormals(ref tm, ref NormalNodes);
				}
				if(parameters.CalcTangents){
					CalcTangents(ref tm);
				}
				;
				tm.RecalculateBounds();
				parameters.Frames[f] = tm;
				float _progress = (float)f/(float)pc.FramesCount;
				progress = .6f+_progress*.2f;
				EditorUtility.DisplayProgressBar("Importing", "Build frames sequence "+f.ToString(), progress);
			}

			if(createMaterials){
				Material[] mats = new Material[SubMeshes.Count];
				Color[] matColors = new Color[]{
					new Color(0.787f, 0.393f, 0.393f),
					new Color(0.291f, 0.765f, 0.141f),
					new Color(0.986f, 1.000f, 0.000f),
					new Color(1.000f, 0.228f, 0.000f),
					new Color(0.241f, 0.000f, 1.000f),
					new Color(1.000f, 0.000f, 0.724f),
					new Color(0.949f, 0.615f, 0.516f)
				};
				
				for(int i = 0; i<mats.Length; i++){
					Material m = new Material(Shader.Find("Diffuse"));
					m.name = SubMeshes[i].MaterialName;
					m.color = matColors[i%matColors.Length];
					string p = Path.GetDirectoryName(parameters.FramesDataPath)+"/"+m.name+".mat";
					AssetDatabase.CreateAsset(m, p);
					mats[i] = m;
				}

				parameters.ThisController.MR.sharedMaterials = mats;
			}

			for(int i = 0; i<  parameters.Frames.Length; i++){
				AssetDatabase.AddObjectToAsset( parameters.Frames[i], parameters.FramesDataPath );
				float _progress = (float)i/(float)parameters.Frames.Length;
				progress = .8f+_progress*.1f;
				EditorUtility.DisplayProgressBar("Importing", "Saving frames "+i.ToString(), progress);
			}
			AssetDatabase.SaveAssets();
			
			Object[] assets = AssetDatabase.LoadAllAssetsAtPath(parameters.FramesDataPath);
			parameters.Frames = new Mesh[assets.Length-1];
			for(int i = 0; i<assets.Length; i++){
				int index = -1;
				Mesh m = (Mesh)assets[i];
				if(m.name.StartsWith("frame #")){
					int.TryParse(m.name.Remove(0,7), out index);
					index = parameters.ReverseTime? (parameters.Frames.Length-index-1) : index; 
					parameters.Frames[index] =  m;
					float _progress = (float)i/(float)parameters.Frames.Length;
					progress = .9f+_progress*.1f;
					EditorUtility.DisplayProgressBar("Importing", "Linking "+i.ToString(), progress);
				}
			}


			EditorUtility.ClearProgressBar();
			AssetDatabase.Refresh();
		}
 	 }


}
