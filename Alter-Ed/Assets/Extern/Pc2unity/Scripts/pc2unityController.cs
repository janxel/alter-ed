using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class pc2unityController : MonoBehaviour {
	public enum PlaybackTypeEnum{
		Once, 
		Loop,
		PingPong,
		None 
	}

	public MeshFilter MF;
	public MeshRenderer MR;
	public PlaybackTypeEnum PlaybackType;
	public int FramesPerSecond;
	public float MinFrame;
	public float MaxFrame;
	public float Frame;
	int playbackDirection = 1;
	float timer;
	float frameDuration;
	public pc2unityImportSettings Parameters;

	void Update(){
		if(Application.isPlaying){
			frameDuration = 1f/FramesPerSecond;
			if(PlaybackType!=PlaybackTypeEnum.None){
				timer += Time.deltaTime;
			}
			if(PlaybackType == PlaybackTypeEnum.Loop){
				playbackDirection = 1;
			}
			if(timer>frameDuration){
				Frame += (int)(timer/frameDuration)*playbackDirection;
				timer = 0;
			}
			if(Frame >= MaxFrame){
				if(PlaybackType == PlaybackTypeEnum.Loop){
					Frame = MinFrame;
				} else if(PlaybackType == PlaybackTypeEnum.Once){
					PlaybackType = PlaybackTypeEnum.None;
				} else if (PlaybackType == PlaybackTypeEnum.PingPong){
					Frame -=2;
					playbackDirection = -1;
				}
			} else if(Frame <= MinFrame ){
				if (PlaybackType == PlaybackTypeEnum.PingPong){
					Frame +=2;
					playbackDirection = 1;
				}
			}
		}
		Frame = Mathf.Clamp(Frame, MinFrame, MaxFrame);
		MF.mesh = Parameters.Frames [(int)Frame];
	}


	public int GetFramesCount(){
		return Parameters.Frames.Length-1;
	}

	public void Init(Material[] Mats){
		if(MF == null){
			MF = GetComponent<MeshFilter>();
		}
		if(MR == null){
			MR = GetComponent<MeshRenderer>();
		}
		MR.materials = Mats;
	}
}

[System.Serializable]
public class pc2unityImportSettings {
	public Transform ThisTransform;
	public pc2unityController ThisController;
	public string ObjFilePath;
	public string PC2FilePath;
	public string FramesDataGuid;
	public string FramesDataPath;
	public string OutputName;

	public bool YUp;
	public bool CalcNormals;
	public bool CalcTangents;
	public Mesh[] Frames;
	public float ScaleFactor;
	public bool ReverseTime;
	public Vector3 PivotOffset;
	public Vector3 PivotRotation;
}
