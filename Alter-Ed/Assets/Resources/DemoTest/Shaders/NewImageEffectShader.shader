﻿Shader "Hidden/NewImageEffectShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Value1("Value 1", Float) = 0
		_Value2("Value 2", Float) = 0
		_Value3("Value 3", Float) = 0
	}
	SubShader
	{
		// No culling or depth
		//Cull Off ZWrite Off ZTest Always

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc" // for _LightColor0

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				fixed4 diff : COLOR0; // diffuse lighting color
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				//float4 color : COLOR;
				fixed4 diff : COLOR0; // diffuse lighting color
			};

			// User defined variables
			uniform float _Value1;
			uniform float _Value2;
			uniform float _Value3;

			v2f vert (appdata v)
			{
				v2f o;
				//
				v.vertex.x += sin((v.vertex.y + _Time * _Value3) * _Value2) * _Value1;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;

				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				o.diff = nl * _LightColor0;

				o.diff.rgb += ShadeSH9(half4(worldNormal, 1));
				return o;
			}
			
			sampler2D _MainTex;
			

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				col *= i.diff;
				return col;
			}
			ENDCG
		}
	}
}
