﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchNormal : SwitchBase, Interactive {

    public Animator animator;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        if (animator != null) animator.SetInteger("State", (int)currentState);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void EmotionAction(GameObject player, Emotion.State newState) {
        
    }
    public override void ApplyLinkedObjectsAction() {
        base.ApplyLinkedObjectsAction();
        if (animator != null) {
            animator.SetInteger("State", (int)this.currentState);
        }
    }
    public void PlayerAction(Emotion.State currentState) {
        Action();
    }
}
