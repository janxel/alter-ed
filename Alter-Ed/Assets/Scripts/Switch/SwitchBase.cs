﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchBase : InteractiveBehaviour {

    public enum State {
        ON = 1,
        OFF = -1
    };

    public GameObject[] linkedObjects;
    public Emotion.State emotionLinked = Emotion.State.None;
    public bool isLoopable = false;
    private bool firstAction = false;
    public State currentState = State.OFF;
	// Use this for initialization
	void Start () {
        firstAction = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void ApplyLinkedObjectsAction() {
        foreach(GameObject obj in linkedObjects) {
            SwitchObjectBase switchObject = obj.GetComponent<SwitchObjectBase>();
            if (switchObject != null && switchObject.isActiveAndEnabled) {
                //LOOK IF IT NEEDS REFACTORIZATION, BECAUSE, MAYBE FOR SWITCHFloor you can activate it when you want it ???
                //LOOK IF IT NEEDS REFACTORIZATION, BECAUSE, MAYBE FOR SWITCHFloor you can activate it when you want it ???
                if ((int)switchObject.currentState == (int)currentState) 
                    switchObject.Action();
                //LOOK IF IT NEEDS REFACTORIZATION, BECAUSE, MAYBE FOR SWITCHFloor you can activate it when you want it ???
                //LOOK IF IT NEEDS REFACTORIZATION, BECAUSE, MAYBE FOR SWITCHFloor you can activate it when you want it ???
            }
        }
        currentState = (State)((int)currentState * -1);
        firstAction = true;
    }

    public void Action() {
        //It works only if its the first action or the switch is loopable
        if (!firstAction || isLoopable) {
            if (emotionLinked == Emotion.State.None) {
                ApplyLinkedObjectsAction();
            } else {
                EmotionController playerEmotion = GameObject.Find("Player").GetComponent<EmotionController>();
                if (playerEmotion.isActiveAndEnabled) {
                    if (emotionLinked == playerEmotion.currentEmotion) {
                        ApplyLinkedObjectsAction();
                    }
                }
            }
        }
    }
}
