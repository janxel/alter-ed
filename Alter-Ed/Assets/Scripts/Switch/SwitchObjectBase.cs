﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchObjectBase : MonoBehaviour {

    //[Flags]
    public enum State {
        OFF = -1, // 0
        ON = 1 // 1
    }
    public State currentState = State.OFF;

    void Start() { 
        
    }
    /*void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) Action();
    }*/
    public void Action() {
        currentState = (State)((int)currentState*-1);
        switch (currentState) {
            case State.OFF: ActionOff(); break;
            case State.ON: ActionOn(); break;
        } 
    }

    /*Impletent always these 2*/
    public virtual void ActionOff() {}
    public virtual void ActionOn() {}

}
