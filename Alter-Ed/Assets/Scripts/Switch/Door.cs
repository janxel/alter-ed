﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : SwitchObjectBase {

    public Animator animator;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    /*Impletent always these 2*/
    public override void ActionOff() {
        animator.SetBool("OpenDoor", false);
    }
    public override void ActionOn() {
        animator.SetBool("OpenDoor", true);
    }
}
