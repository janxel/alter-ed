﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchFloor : SwitchBase {

    public float reEnableTime = 1;
    public float reEnableCurrentTime = 0;
    public bool isEnable = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!isEnable) {
            reEnableCurrentTime += Time.deltaTime;
            if (reEnableCurrentTime >= reEnableTime) {
                reEnableCurrentTime = 0;
                isEnable = true;
            }
        }
	}

    void OnTriggerEnter(Collider other) {
        if (isEnable) Action();
        isEnable = false;
        //StartCoroutine(ReEnable());
        
    }
    IEnumerator ReEnable() {
        isEnable = true;
        yield return new WaitForSeconds(reEnableTime);
    }
    void OnTriggerExit(Collider other) {
        
    }
}
