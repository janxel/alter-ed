﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestController : MonoBehaviour {

    public GameObject cameraParams;
    private bool startRestart;
    public string sceneName = "Test";
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        CameraParams();
        RestartScene();
	}

    void RestartScene() {
        if (Input.GetButtonDown("Restart")) {
            if (!startRestart) {
                startRestart = true;
                SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
            }
        }
    }
        

    void CameraParams() {
        //Show/Hide camera params
        if (Input.GetButtonDown("HideParams")) {
            if (cameraParams != null) {
                cameraParams.SetActive(!cameraParams.activeSelf);
            }
        }
    }
}