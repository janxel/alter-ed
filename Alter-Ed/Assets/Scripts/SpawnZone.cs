﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZone : MonoBehaviour {

    public GameObject player;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null) {
            player.SetActive(true);
            player.transform.position = transform.position;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
