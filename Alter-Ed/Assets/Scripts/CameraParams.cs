﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraParams : MonoBehaviour {

    public Text fov;
    private Slider fovSlider;
    public Text targetDistance;
    private Slider targetDistanceSlider;
    public Text smoothNess;
    public Text orbitSpeed;
    private Slider orbitSpeedSlider;
    public Text xRotation;
    private Slider xRotationSlider;
    public Text yRotation;
    private Slider yRotationSlider;
    public TopDownCamera topDownCamera;
    private bool firstTime;
	// Use this for initialization
	void Start () {
        topDownCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<TopDownCamera>();
        fovSlider = GameObject.Find("FovSlider").GetComponent<Slider>() ;
        xRotationSlider = GameObject.Find("XRotationSlider").GetComponent<Slider>();
        yRotationSlider = GameObject.Find("YRotationSlider").GetComponent<Slider>();
        targetDistanceSlider = GameObject.Find("TargetDistanceSlider").GetComponent<Slider>();
        orbitSpeedSlider = GameObject.Find("OrbitSpeedSlider").GetComponent<Slider>();
        SetupCameraParams();
    }
	
    void SetupCameraParams() {
       // Debug.Log(Screen.width + " , "+ Screen.height);
    }

    // Update is called once per frame
    void Update () {
        if (!firstTime) {
            firstTime = true;
            fov.text = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().fieldOfView.ToString();
            fovSlider.value = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().fieldOfView;
            if (topDownCamera != null) {
                targetDistance.text = topDownCamera.position.distanceFromTarget.ToString();
                targetDistanceSlider.value = topDownCamera.position.distanceFromTarget;

                xRotation.text = topDownCamera.orbit.xRotation.ToString();
                xRotationSlider.value = topDownCamera.orbit.xRotation;

                yRotation.text = topDownCamera.orbit.yRotation.ToString();
                yRotationSlider.value = topDownCamera.orbit.yRotation;

                orbitSpeed.text = topDownCamera.orbit.orbitSpeedController.ToString();
                orbitSpeedSlider.value = topDownCamera.orbit.orbitSpeedController;
            }
        }
	}
    public void ChangeSliderFov(float newValue) {
        fov.text = newValue.ToString();
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().fieldOfView = newValue;
    }
    public void ChangeXRotationSlider(float newValue) {
        xRotation.text = newValue.ToString();
        if (topDownCamera != null)
            topDownCamera.orbit.xRotation = newValue;
        else
            throw new System.Exception("Please Install the TopDownCamera");
    }
    public void ChangeYRotationSlider(float newValue) {
        yRotation.text = newValue.ToString();
        if (topDownCamera != null)
            topDownCamera.orbit.yRotation = newValue;
        else
            throw new System.Exception("Please Install the TopDownCamera");
    }

    public void ChangeTargetDistanceSlider(float newValue) {
        targetDistance.text = newValue.ToString();
        if (topDownCamera != null)
            topDownCamera.position.distanceFromTarget = newValue;
        else
            throw new System.Exception("Please Install the TopDownCamera");
    }

    public void ChangeOrbitSpeedSlider(float newValue) {
        orbitSpeed.text = newValue.ToString();
        if (topDownCamera != null)
            topDownCamera.orbit.orbitSpeedController = newValue;
        else
            throw new System.Exception("Please Install the TopDownCamera");
    }
}