﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalZone : MonoBehaviour {

    public bool isFinishedGame;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
    void OnTriggerEnter(Collider other) {
        if (!isFinishedGame) {
            if (other.tag == "Player") {
                Debug.Log("You Win!!!!!");
                isFinishedGame = true;
            }
        }
    }
}
