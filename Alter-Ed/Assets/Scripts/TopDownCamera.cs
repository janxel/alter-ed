﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCamera : MonoBehaviour {

    public Transform target;

	[System.Serializable]
    public class PositionSettings {
        //Distance from our target
        //Bools for zooming and smoothfollowing
        //Min and max zoom settings

        public float distanceFromTarget = -50;
        public bool allowZoom = true;
        public float zoomSmooth = 100;
        public float zoomStep = 2;
        public float maxZoom = -30;
        public float minZoom = -60;
        public bool smoothFollow = true;
        public float smooth = 0.05f;

        [HideInInspector]
        public float newDistance = -50;//used for smooth zooming gives us a "destination" zoom
    }

    [System.Serializable]
    public class OrbitSettings {
        //Holding our current x and y rotation for our camera
        //Bool for allowing orbit
        public float xRotation = -65;
        public float yRotation = -180;
        public bool allowOrbit = true;
        public float yOrbitSmooth = 0.5f;
        public float orbitSpeedController = 5;
    }

    [System.Serializable]
    public class InputSettings {
        public string MOUSE_ORBIT = "MouseOrbit";
        public string CONTROL_ORBIT_RIGHT = "CameraTurnRight";
        public string CONTROL_ORBIT_LEFT = "CameraTurnLeft";
        public string ZOOM = "Mouse ScrollWheel";
    }

    public PositionSettings position = new PositionSettings();
    public OrbitSettings orbit = new OrbitSettings();
    public InputSettings input = new InputSettings();

    Vector3 destination = Vector3.zero;
    Vector3 camVelocity = Vector3.zero;
    Vector3 currentMousePosition = Vector3.zero;
    Vector3 previousMousePosition = Vector3.zero;
    float yOrbit, mouseOrbitInput, zoomInput, joystickOrbitInput;
    
    void Start() {
        //Setting camera target
        SetCameraTarget(target);
        if (target) {
            MoveTarget();
        }
    }

    public void SetCameraTarget(Transform t) {
        //If we want to set a new target at runtime
        target = t;
        if(target == null) {
            Debug.LogError("Your Camera needs a target");
        }
    }
    
    void GetInput() {
        //Filling the values for our input variables

        // joystickOrbitInput = Input.GetAxisRaw(input.CONTROL_ORBIT_RIGHT);
        joystickOrbitInput = (-1 * Input.GetAxisRaw(input.CONTROL_ORBIT_RIGHT)) + Input.GetAxisRaw(input.CONTROL_ORBIT_LEFT);
       // Debug.Log(joystickOrbitInput + ": " + Input.GetAxisRaw(input.CONTROL_ORBIT_LEFT) + " , " + (-1 * Input.GetAxisRaw(input.CONTROL_ORBIT_RIGHT)));
        mouseOrbitInput = Input.GetAxisRaw(input.MOUSE_ORBIT);
        zoomInput = Input.GetAxisRaw(input.ZOOM);
    }

    void Update() {
        //Getting input 
        //Zooming
        GetInput();
        if (position.allowZoom) {
            ZoomInOnTarget();
        }
    }

    void FixedUpdate() {
        //moveToTarget
        //lookAtTarget
        //orbit
        if (target) {
            MoveTarget();
            LookAtTarget();
            MouseOrbitTarget();
            ControlOrbitTarget();
        }
    }

    void MoveTarget() {
        //Handling getting our camera to its destination position
        destination = target.position;
        destination += Quaternion.Euler(orbit.xRotation, orbit.yRotation, 0) * -Vector3.forward * position.distanceFromTarget;

        if (position.smoothFollow) {
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref camVelocity, position.smooth);
        }else {
            transform.position = destination;
        }
    }

   
    void LookAtTarget() {
        //Handling getting our camera to look at the target at all times
        Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);
        transform.rotation = targetRotation;
    }

    void ControlOrbitTarget() {
        if(joystickOrbitInput > 0)
            orbit.yRotation += orbit.orbitSpeedController * orbit.yOrbitSmooth;
        if (joystickOrbitInput < 0)
            orbit.yRotation -= orbit.orbitSpeedController * orbit.yOrbitSmooth;
    }

    void MouseOrbitTarget() {
        //Getting the camera to orbit around our character
        previousMousePosition = currentMousePosition;
        currentMousePosition = Input.mousePosition;
        if (mouseOrbitInput > 0) {
            if(currentMousePosition.y<Screen.height*0.5f)
                orbit.yRotation += (currentMousePosition.x - previousMousePosition.x) * orbit.yOrbitSmooth;
        }
       // Debug.Log(currentMousePosition.x+" , "+currentMousePosition.y);
    }

    void ZoomInOnTarget() {
        //Modifying the distance from target to be closer or further away from our target
        position.newDistance += position.zoomStep * zoomInput;
        position.distanceFromTarget = Mathf.Lerp(position.distanceFromTarget, position.newDistance, position.zoomSmooth * Time.deltaTime);

        if (position.distanceFromTarget > position.maxZoom) {
            position.distanceFromTarget = position.maxZoom;
            position.newDistance = position.maxZoom;
        }
        if (position.distanceFromTarget < position.minZoom) {
            position.distanceFromTarget = position.minZoom;
            position.newDistance = position.minZoom;
        }
    }
}
