﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public Vector3 velocity = Vector3.zero;
    public Vector3 velocityToRun = new Vector3(0.75f, 0, 0.75f);
    public float velocityForce = 10;
    public Animator playerAnimator;
    public float moveSpeed = 5;
    public float rotationSpeed = 6;

    public GameObject currentObject; 

    public bool startJump;
    public float jumpForce = 5;

    public JumpController jumpController;
    public EmotionController emotionController;
    public bool c_Jump;
    // Use this for initialization
    void Start () {
        playerAnimator = GameObject.FindGameObjectWithTag("PlayerModel").GetComponent<Animator>();
        jumpController = GetComponent<JumpController>();
        emotionController = GetComponent<EmotionController>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        
        RotatePlayer();
        MovePlayer();
        //NEED REFACTORIZATION//NEED REFACTORIZATION
        if (jumpController != null) {
            if (emotionController.isActiveAndEnabled) {
                if(jumpController.isActiveAndEnabled) jumpController.Move(c_Jump);
                c_Jump = false;
            }
        }
        //NEED REFACTORIZATION//NEED REFACTORIZATION
    }

    void Update() {
        CheckInput();
    }
    

    void RotatePlayer() {
        Vector3 cameraRotation = GameObject.FindGameObjectWithTag("MainCamera").transform.eulerAngles;
        cameraRotation.x = 0;
        Vector3 movement = new Vector3(velocity.x, 0, velocity.y);
        Quaternion direction = new Quaternion();
        if (movement != Vector3.zero) {
            direction = Quaternion.LookRotation(movement, Vector3.up) * Quaternion.Euler(0, cameraRotation.y, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, direction, rotationSpeed * Time.deltaTime);
        }
    }

    void MovePlayer() {
        if (velocity.x > velocityToRun.x || velocity.x < -velocityToRun.x || velocity.y > velocityToRun.y || velocity.y < -velocityToRun.y) {
            Vector3 translation = (transform.forward) * 5 * Time.deltaTime;
            transform.Translate(translation, Space.World);
        }
    }

    void CheckInput() {
        velocity.x = Input.GetAxis("Horizontal");
        velocity.y = Input.GetAxis("Vertical");
        if (velocity.x == 0 && velocity.y == 0) {
            playerAnimator.SetBool("Run", false);
        } else {
            playerAnimator.SetBool("Run", true);
        }

        //NEED REFACTORIZATION//NEED REFACTORIZATION
        if (emotionController != null) {
            if(emotionController.isActiveAndEnabled && emotionController.currentEmotion == Emotion.State.Fear) {
                if (!c_Jump) {
                    c_Jump = Input.GetButtonDown("Jump");
                }
            }
        }
        //NEED REFACTORIZATION//NEED REFACTORIZATION
    }
}
