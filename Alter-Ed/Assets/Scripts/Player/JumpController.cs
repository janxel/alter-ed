﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpController : MonoBehaviour {

   

    public Rigidbody c_RigidBody;
    public Animator c_Animator;
    public float c_JumpPower = 12f;
    public bool c_IsGrounded = true;
    public float c_GroundCheckDistance = 0.1f;
    public float c_OrigGroundCheckDistance;
    Vector3 m_GroundNormal;
    [Range(1f, 4f)] [SerializeField] float c_GravityMultiplier = 2f;
    private bool c_Jump;

    void Start () {
        c_RigidBody = GetComponent<Rigidbody>();
        c_Animator = GetComponentInChildren<Animator>();
        c_OrigGroundCheckDistance = c_GroundCheckDistance;
        c_RigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
    }
    

    public void Move(bool jump) {
        CheckGroundStatus();
        if (c_IsGrounded) {
            HandleGroundedMovement(jump);
        } else {
            HandleAirborneMovement();
        }
    }

    void HandleGroundedMovement(bool jump) {
        // check whether conditions are right to allow a jump:
        if (jump) {
            c_RigidBody.velocity = new Vector3(c_RigidBody.velocity.x, c_JumpPower, c_RigidBody.velocity.z);
            c_IsGrounded = false;
            c_GroundCheckDistance = 0.1f;
        }
    }

    void HandleAirborneMovement() {
        // apply extra gravity from multiplier:
        Vector3 extraGravityForce = (Physics.gravity * c_GravityMultiplier) - Physics.gravity;
        c_RigidBody.AddForce(extraGravityForce);

        c_GroundCheckDistance = c_RigidBody.velocity.y < 0 ? c_OrigGroundCheckDistance : 0.01f;
    }

    void CheckGroundStatus() {
        RaycastHit hitInfo;
        #if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * c_GroundCheckDistance));
        #endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, c_GroundCheckDistance)) {
            //m_GroundNormal = hitInfo.normal;
            c_IsGrounded = true;
            c_Animator.applyRootMotion = true;
        } else {
            c_IsGrounded = false;
            //m_GroundNormal = Vector3.up;
            c_Animator.applyRootMotion = false;
        }
    }
}
