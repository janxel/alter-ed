﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEmotionController : MonoBehaviour {
    public GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("ChangeEmotion")) {
            player.GetComponent<EmotionController>().ChangeEmotion();
            ChangeColor();
        }
	}

    public void ChangeColor() {
        SkinnedMeshRenderer mesh = GetComponentInChildren<SkinnedMeshRenderer>();
        mesh.material.shader = Shader.Find("Standard");
        //Blue: Sadness, Red: Anger, Orange: Joy, Purple: Fear
        switch (player.GetComponent<EmotionController>().currentEmotion) {
            case Emotion.State.Joy:
                mesh.material.SetColor("_Color", new Color(1, 121f/255f, 0, 1));
                break;
            case Emotion.State.Anger:
                mesh.material.SetColor("_Color", Color.red);
            break;
            case Emotion.State.Fear:
                mesh.material.SetColor("_Color", new Color(167f / 255f, 0, 1, 1));
            break;
            case Emotion.State.Sadness:
                mesh.material.SetColor("_Color", Color.blue);
            break;
        }
        
    }
}
