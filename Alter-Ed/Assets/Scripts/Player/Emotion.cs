﻿using System.Collections;
using System.Collections.Generic;
using System;

public class Emotion {
    [Flags]
    public enum State {
        Joy = 1 << 0,     // 000000
        Sadness = 1 << 1, // 000001
        Anger = 1 << 2,   // 000010
        Fear = 1 << 3,    // 000100
        None = 1 << 4     // 001000
    };
}
