﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionController : MonoBehaviour {

    
    public Emotion.State currentEmotion;
    
    /*
    public enum States {
        Joy = 1,
        Sadness = 2,
        Anger = 3,
        Fear = 4
    };
    */
    public int mentalHealth;
    public GameObject currentObject; // The object the player has collided, must have Interactive interface
    public bool canAffectAllInteractive = true;

    public enum PlayerState {
        Alive = 1,
        Inmovil = 2,
        Death = 3
    };
    public PlayerState currentPlayerState;

    void AffectAllObjects() {
        //Affect all the objects when player change of emotion
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Interactive");
        foreach (GameObject item in objects) {
            if (item.GetComponent<Interactive>() != null) {
                item.GetComponent<Interactive>().EmotionAction(gameObject, currentEmotion);
            }
        }
    }

	// Use this for initialization
	void Start () {
        currentEmotion = Emotion.State.Joy;
        currentPlayerState = PlayerState.Alive;
        mentalHealth = PlayerData.MAX_MENTAL_HEALTH;
        SetEmotion(currentEmotion);
    }

    public void UpdateMentalHealth(int value) {
        mentalHealth += value;
        if (mentalHealth <= 0) {
            Debug.Log("YOU ARE DEATH!");
            currentPlayerState = PlayerState.Death;
        }
    }

    public void SetEmotion(Emotion.State newState) {
        currentEmotion = newState;
        //Debug.Log(newState);
        if (canAffectAllInteractive) AffectAllObjects();
    }

    public void ChangeEmotion() {
        if (currentEmotion == Emotion.State.Fear) {
            
            SetEmotion(Emotion.State.Joy);
        } else {
           SetEmotion((Emotion.State)((int)currentEmotion << 1));
           
        }
    }

    void Update() {
        if (Input.GetButtonDown("Action")) {
            if (currentObject != null) {
                if(currentObject.activeSelf) currentObject.GetComponent<Interactive>().PlayerAction(currentEmotion);
            }
        }
    }
}