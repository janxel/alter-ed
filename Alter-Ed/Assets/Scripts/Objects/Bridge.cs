﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : InteractiveBehaviour, Interactive {

    public Animator animationController;
    public Emotion.State emotionToFall = Emotion.State.Anger;
	// Use this for initialization
	void Start () {
        animationController = transform.parent.GetComponent<Animator>();
	}

    public void PlayerAction(Emotion.State currentState) {
        if (currentState == emotionToFall) {
            if (!actionExecuted) {
                actionExecuted = true;
                animationController.SetBool("Fall", true);
            }
            
        }
    }

    public void EmotionAction(GameObject player, Emotion.State newState) {

    }
}
