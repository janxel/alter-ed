﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBreak : InteractiveBehaviour, Interactive {

    private GameObject parent;
	// Use this for initialization
	void Start () {
        if (transform.parent != null)
            parent = transform.parent.gameObject;
        else
            parent = gameObject; //Itself is its father
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public void PlayerAction(Emotion.State currentState) {
        if (currentState == Emotion.State.Anger) {
            actionExecuted = true;
            parent.SetActive(false);
        }
    }

    public void EmotionAction(GameObject player, Emotion.State newState) {
        
    }
}