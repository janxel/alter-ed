﻿using UnityEngine;
public interface Interactive {

    //Action made by the player
	void PlayerAction(Emotion.State currentState);

    //Action made by the emotion
    void EmotionAction(GameObject player, Emotion.State newState);
}
