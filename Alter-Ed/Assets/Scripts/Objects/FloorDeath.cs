﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorDeath : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other) {
        if (other.name == "Player") {
            if (other.GetComponent<EmotionController>()) {
                other.GetComponent<EmotionController>().UpdateMentalHealth(-PlayerData.MAX_MENTAL_HEALTH);
            }
        }
    }
}
