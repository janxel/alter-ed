﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveBehaviour : MonoBehaviour {

    public bool actionExecuted;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    void OnTriggerEnter(Collider other) {
        if (other.name == "Player") {
            other.GetComponent<EmotionController>().currentObject = gameObject;
        }
    }
    void OnTriggerExit(Collider other) {
        if (other.name == "Player") {
            other.GetComponent<EmotionController>().currentObject = null;
        }
    }
}