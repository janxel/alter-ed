﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorBase : MonoBehaviour, Interactive {

    public GameObject floor;

    public void EmotionAction(GameObject player, Emotion.State newState) {
        EmotionController emo = player.GetComponent<EmotionController>();
        if (emo.isActiveAndEnabled) {
            if (emo.currentEmotion != Emotion.State.Fear) {
                floor.GetComponent<BoxCollider>().isTrigger = true;
            }else {
                floor.GetComponent<BoxCollider>().isTrigger = false;
            }
        }
    }

    public void PlayerAction(Emotion.State currentState) {
        //throw new NotImplementedException();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
