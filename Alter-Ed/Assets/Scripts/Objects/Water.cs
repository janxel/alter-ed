﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour,Interactive {

    public void EmotionAction(GameObject player, Emotion.State newState) {
        if (player.GetComponent<EmotionController>().currentEmotion == Emotion.State.Sadness) {
            transform.parent.GetComponent<BoxCollider>().isTrigger = true;
        }else {
            transform.parent.GetComponent<BoxCollider>().isTrigger = false;
        }
    }

    public void PlayerAction(Emotion.State currentState) {
        throw new NotImplementedException();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /*void OnTriggerEnter(Collider other) {
        if (other.name == "Player") {
           if(other.GetComponent<EmotionController>().currentEmotion == Emotion.States.Sadness) {
                transform.parent.GetComponent<BoxCollider>().isTrigger = true;
            }
        }
    }
    void OnTriggerExit(Collider other) {
        if (other.name == "Player") {
            if (other.GetComponent<EmotionController>().currentEmotion == Emotion.States.Sadness) {
                transform.parent.GetComponent<BoxCollider>().isTrigger = true;
            }
        }
    }*/
}
