﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class NPCGeneralBehaviour : MonoBehaviour {

    public enum State {
        Idle,
        Run,
        Attack
    };
    public State currentState;
    public Animator animController;
    public Transform target;
    public float timeToRecalculatePath = 1f;
    public float currentTime = 0;
    private NavMeshAgent agent;
    private NavMeshPath path;
    public NavMeshPathStatus lastPathStatus;
    // Use this for initialization
    void Start() {
        animController = transform.GetChild(0).GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        path = new NavMeshPath();
        ChangeState(State.Idle);
        SetTarget(GameObject.Find("Player").transform);
    }
    public string GetStateString(State state) {
        string statestr = "";
        switch (state) {
            case State.Idle:
                statestr = "Idle";
                break;
            case State.Attack:
                statestr = "Attack";
                break;
            case State.Run:
                statestr = "Run";
                break;
            default:
                statestr = "Idle";
                break;

        }
        return statestr;
    }
    void ChangeState(State newState) {
        currentState = newState;
        if (animController != null)
            animController.SetTrigger(GetStateString(currentState));
    }
    // Update is called once per frame
    void Update() {
        UpdatePathRecalculation();
    }

    void SetTarget(Transform target) {
        if (target != null) {
            this.target = target;
        }
    }

    void UpdatePathRecalculation() {
        currentTime += Time.deltaTime;
        if (currentTime > timeToRecalculatePath) {
                NavMesh.CalculatePath(transform.position, target.position, NavMesh.AllAreas, path);
            if(path.status == NavMeshPathStatus.PathComplete) {
                agent.SetPath(path);
                ChangeState(State.Run);
            } else {
                if (agent.hasPath)
                    agent.ResetPath();
                ChangeState(State.Idle);
            }
            lastPathStatus = path.status;
            currentTime = 0;
        }
        if (agent.hasPath) {
            for (int i = 0; i < path.corners.Length - 1; i++)
                Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
        }
    }
    
}
