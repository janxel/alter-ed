﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionZone : MonoBehaviour {

    public Emotion.State emotion = Emotion.State.None;
    public float timeToStart;
    public int repeat = 1;
    private int currentRepeat = 0;
    public bool testMode;
	// Use this for initialization
	void Start () {
        if (!testMode) {
            GetComponent<MeshRenderer>().enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void LaunchEmotionTrigger() {
        Invoke("ChangeEmotion", timeToStart);
    }

    void ChangeEmotion() {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null) {
            if(player.GetComponent<EmotionController>().currentPlayerState != EmotionController.PlayerState.Death) {
                player.GetComponent<EmotionController>().SetEmotion(emotion);
                TestEmotionController testEmotion = player.GetComponent<TestEmotionController>();
                if (testEmotion != null) testEmotion.ChangeColor();
            }
        }
    }
    void OnTriggerEnter(Collider other) {
        if(other.gameObject.name == "Player") {
            if(other.GetComponent<EmotionController>().currentEmotion != emotion) {
                if (currentRepeat < repeat) {
                    LaunchEmotionTrigger();
                    currentRepeat++;
                }
            }
        }
    }
}
